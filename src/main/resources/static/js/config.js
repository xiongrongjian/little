function ToastError(msg,duration){
    duration=isNaN(duration)?3000:duration;
    var m = document.createElement('div');
    m.innerHTML = "操作失败:"+msg;
    m.style.cssText="max-width:60%;min-width: 60%;max-hegiht:60%;min-hegiht:10%;color: rgb(255,20,147);line-height: 80px;text-align: center;border-radius: 4px;position: fixed;top: 5%;left: 50%;transform: translate(-50%, -50%);z-index: 999999;background: rgb(248,248,255);font-size: 30px;";
    document.body.appendChild(m);
    setTimeout(function() {
        var d = 0.5;
        m.style.webkitTransition = '-webkit-transform ' + d + 's ease-in, opacity ' + d + 's ease-in';
        m.style.opacity = '0';
        setTimeout(function() { document.body.removeChild(m) }, d * 1000);
    }, duration);
}

function  isPrice(price) {
    var priceReg = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
    if (!priceReg.test(price)){
        //请输入正确的产品价格:整数或者保留两位小数
        return false;
    }
    return true
}

function ToastSuccess(){
    duration=3000;
    var m = document.createElement('div');
    m.innerHTML = "操作成功";
    m.style.cssText="max-width:60%;min-width: 60%;max-hegiht:60%;min-hegiht:10%;color: rgb(240,248,255);line-height: 80px;text-align: center;border-radius: 4px;position: fixed;top: 5%;left: 50%;transform: translate(-50%, -50%);z-index: 999999;background: rgba(91,192,222,1);font-size: 35px; border-color: rgb(70, 184, 218)";
    document.body.appendChild(m);
    setTimeout(function() {
        var d = 0.5;
        m.style.webkitTransition = '-webkit-transform ' + d + 's ease-in, opacity ' + d + 's ease-in';
        m.style.opacity = '0';
        setTimeout(function() { document.body.removeChild(m) }, d * 1000);
    }, duration);
}
