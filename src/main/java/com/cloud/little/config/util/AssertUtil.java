package com.cloud.little.config.util;

import com.cloud.little.config.exception.handler.BusinessException;

/**
 * 断言
 */
public class AssertUtil {
    public static void isNotNull(Object o, String errmsg) {
        if (o == null || o.toString().length() == 0) {
            throw new BusinessException(errmsg);
        }
    }

    public static void isNull(Object o, String errmsg) {
        if (o != null) {
            throw new BusinessException(errmsg);
        }
    }

}
