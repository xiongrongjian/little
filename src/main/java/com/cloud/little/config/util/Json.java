package com.cloud.little.config.util;

import com.alibaba.fastjson.JSONObject;

public class Json {


    public static JSONObject success(String errcode, String errmsg, Object data) {
        JSONObject jb = new JSONObject();
        jb.put("errcode", errcode);
        jb.put("errmsg", errmsg);
        jb.put("data", data);
        return jb;
    }

    public static JSONObject success(Object data) {
        JSONObject jb = new JSONObject();
        jb.put("errcode", "0");
        jb.put("data", data);
        return jb;
    }


    public static JSONObject success() {
        JSONObject jb = new JSONObject();
        jb.put("errcode", "0");
        jb.put("errmsg", "ok");
        return jb;
    }


    public static JSONObject fail(String errmsg) {
        JSONObject jb = new JSONObject();
        jb.put("errcode", "-1");
        jb.put("errmsg", errmsg);
        return jb;
    }

    public static JSONObject fail(String errcode, String errmsg, Object data) {
        //new 出的对象没办法调用静态属性或方法
        JSONObject jb = new JSONObject();
        jb.put("errcode", errcode);
        jb.put("errmsg", errmsg);
        jb.put("data", data);
        return jb;
    }

    public static JSONObject fail(String errcode, String errmsg) {
        //new 出的对象没办法调用静态属性或方法
        JSONObject jb = new JSONObject();
        jb.put("errcode", errcode);
        jb.put("errmsg", errmsg);
        return jb;
    }

}
