package com.cloud.little.config.exception.code;


public enum Errcode {
    //   错误码命名规则:5【系统编号(0-9)】【模块编号(0-9)】【错误码(0-9)】【错误码(0-9)】
    //   系统公共:50开头


    /**
     * 登录相关
     */
    LOGIN_001("50001","用户未注册");


    private String errCode;
    private String errMsg;

    Errcode(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
