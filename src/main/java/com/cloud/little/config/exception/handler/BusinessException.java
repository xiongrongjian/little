package com.cloud.little.config.exception.handler;

import com.cloud.little.config.exception.code.Errcode;
import lombok.Data;
import lombok.ToString;

/**
 * @ Author     ：熊荣健
 * @ Date       ：Created in ${TIME} ${DATE}
 * @ Description：
 * @ Modified By：
 * @Version:
 */
@Data
@ToString
public class BusinessException extends RuntimeException {
    private String errorCode;
    private String errorMsg;


    public BusinessException(Errcode errcode) {
        this.errorCode = errcode.getErrCode();
        this.errorMsg = errcode.getErrMsg();
    }

    public BusinessException(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BusinessException(String errorMsg) {
        this.errorCode = "1";
        this.errorMsg = errorMsg;
    }

}
