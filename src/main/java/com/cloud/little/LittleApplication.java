package com.cloud.little;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cloud.little.mapper")
public class LittleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittleApplication.class, args);
    }

}
