package com.cloud.little.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品名称
     */
    @TableField("goodsName")
    private String goodsName;
    /**
     * 价格
     */
    @TableField("price")
    private BigDecimal price;
    /**
     * 品类id
     */
    @TableField("categoryId")
    private Integer categoryId;
    /**
     * 库存
     */
    @TableField("inventory")
    private Integer inventory;
    /**
     * 更新时间
     */
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 创建时间
     */
    @TableField("createTime")
    private Date createTime;


}
