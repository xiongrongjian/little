package com.cloud.little.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cloud.little.entity.Goods;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-08-16
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
