package com.cloud.little.test.staticproxy;

public class Hello implements HelloInterface {
    @Override
    public void sayHello() {
        System.out.println("sayHello");
    }

    @Override
    public void sayBye() {
        System.out.println("sayBye");
    }
}
