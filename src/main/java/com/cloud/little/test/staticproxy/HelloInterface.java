package com.cloud.little.test.staticproxy;

public interface HelloInterface {
    void sayHello();

    void sayBye();
}
