package com.cloud.little.test.staticproxy;

public class HelloProxy implements HelloInterface {
    private HelloInterface helloInterface;

    public HelloProxy(HelloInterface helloInterface) {
        this.helloInterface = helloInterface;
    }

    @Override
    public void sayHello() {
        System.out.println("before");
        helloInterface.sayHello();
        System.out.println("after");
    }

    @Override
    public void sayBye() {
        System.out.println("before");
        helloInterface.sayBye();
        System.out.println("after");
    }

    public static void main(String[] args) {
        new HelloProxy(new Hello()).sayHello();

    }
}
