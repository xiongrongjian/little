package com.cloud.little.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.cloud.little.config.exception.handler.BusinessException;
import com.cloud.little.config.util.AssertUtil;
import com.cloud.little.config.util.Json;
import com.cloud.little.entity.Goods;
import com.cloud.little.mapper.GoodsMapper;
import com.cloud.little.service.IGoodsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author author
 * @since 2020-08-16
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    /**
     * 插入或者更新商品详情，有传入主键则为更新
     */
    @Override
    @Transactional
    public JSONObject addOrUpdateGoods(Goods goods) {



        if (goods.getId() == null) {
            baseMapper.insert(goods);
        } else {
            goods.setUpdateTime(null);
            baseMapper.updateById(goods);
        }
        return Json.success(goods.getId());
    }
}
