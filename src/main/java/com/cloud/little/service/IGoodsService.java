package com.cloud.little.service;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.cloud.little.entity.Goods;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2020-08-16
 */
public interface IGoodsService extends IService<Goods> {

    JSONObject addOrUpdateGoods(Goods goods);

}
